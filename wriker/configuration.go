package main

import (
	"log"
	"os"
	"path"
)

type Config struct {
	AppFolder  string
	DataFolder string
}

func (c *Config) CreateFolders() {
	os.Mkdir(c.AppFolder, os.ModeDir|0755)
	os.Mkdir(c.DataFolder, os.ModeDir|0755)
}

func DefaultConfig() Config {
	homedir, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}
	c := Config{
		AppFolder:  path.Join(homedir, ".wriker"),
		DataFolder: path.Join(homedir, ".wriker", "data"),
	}

	if _, err := os.Stat(c.AppFolder); os.IsNotExist(err) {
		c.CreateFolders()
	}
	return c
}
