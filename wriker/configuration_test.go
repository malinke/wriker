package main_test

import (
	"os"
	"path"
	"testing"

	"gitlab.com/malinke/wriker"
)

func TestDefaultConfig(t *testing.T) {
	c := main.DefaultConfig()
	homedir, err := os.UserHomeDir()
	if err != nil {
		t.Errorf("Could not determine home directory: %v", err)
	}
	appFolder := path.Join(homedir, ".wriker")
	if c.AppFolder != appFolder {
		t.Errorf("expected appfolder as %s", appFolder)
	}
	dataFolder := path.Join(appFolder, "data")
	if c.DataFolder != dataFolder {
		t.Errorf("expected datafolder as %s", dataFolder)
	}
}
