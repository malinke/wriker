module gitlab.com/malinke/wriker

go 1.15

require (
	github.com/rs/xid v1.2.1
	github.com/urfave/cli/v2 v2.3.0
)
