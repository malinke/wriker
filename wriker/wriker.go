package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"time"

	"github.com/rs/xid"
	"github.com/urfave/cli/v2" // imports as package "cli"
)

const (
	Name = "wriker"
)

type Interval struct {
	Start int64
	End   int64
}

type StartedInterval struct {
	Start int64
}

type Slip struct {
	uuid    xid.ID
	start   time.Time
	stop    time.Time
	comment string
	// wrikeId string
	// wrikeTitle string
	// published boolean
}

func Start(config Config) error {
	now := time.Now()
	i := StartedInterval{now.Unix()}
	b, err := json.MarshalIndent(i, "", " ")
	if err != nil {
		return err
	}
	_ = ioutil.WriteFile(path.Join(config.DataFolder, "pending.json"), b, 0644)
	return nil
}

func Export(config Config) []map[string]string {
	return nil
}

func main() {
	// start 9:00 (auto stop previous slip) --offline
	// stop --comment=foobar
	// track 9:00 - 11:00 --comment='foobar' (overlap checks!) --offline
	// cancel
	// continue (create new slip with previous wrike task)
	// export --range=*today* / (can be useful for testing, and input for wrike push internally)
	// summary --range=*today*/yesterday/week/... (create table in cli)
	// *** wrike handling***
	// config --api-token=foobar (for wrike setup)
	// tasks --store-local
	// push --range=*today*/yesterday/... (update time logs for given range)
	// timelogs (show wrike time logs)
	// compare-wrike --range=*today*/...
	// **experimantal ideas**
	// (undo (track changes in git. use it to implement undo)) does it also update wrike???
	// (commands to modify timeslips?)
	app := &cli.App{
		Commands: []*cli.Command{
			{
				Name:  "start",
				Usage: "start tracking",
				Action: func(c *cli.Context) error {
					config := DefaultConfig()
					return Start(config)
				},
			},
			{
				Name:  "export",
				Usage: "export json",
				Action: func(c *cli.Context) error {
					config := DefaultConfig()
					Export(config)
					return nil
				},
			},
			{
				Name:  "stop",
				Usage: "Stop tracking",
				Action: func(c *cli.Context) error {
					fmt.Println("stop")
					return nil
				},
			},
		}}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
