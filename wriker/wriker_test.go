package main_test

import (
	"encoding/json"
	"math/rand"
	"os"
	"path"
	"reflect"
	"testing"
	"time"

	"github.com/rs/xid"
	"gitlab.com/malinke/wriker"
)

const letters = "abcdefghijklmnopqrstuvwxyz"

func RandString(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func createRandomFolder() string {
	tmpdir := os.TempDir()
	folder := "wriker-test-" + RandString(5)
	return path.Join(tmpdir, folder)
}

func createTestConfig() main.Config {
	folder := createRandomFolder()
	c := main.Config{AppFolder: folder, DataFolder: path.Join(folder, "data")}
	c.CreateFolders()
	return c
}

func cleanTestconfig(c main.Config) {
	os.RemoveAll(c.AppFolder)
}

func JsonEqual(a, b []byte) (bool, error) {
	var ja, jb interface{}
	if err := json.Unmarshal(a, &ja); err != nil {
		return false, err
	}
	if err := json.Unmarshal(b, &jb); err != nil {
		return false, err
	}
	return reflect.DeepEqual(ja, jb), nil
}

func TestStart(t *testing.T) {
	c := createTestConfig()
	defer cleanTestconfig(c)
	now := time.Now()
	main.Start(c)
	export := main.Export(c)
	if len(export) != 1 {
		t.Errorf("expected only a single entry exists")
	}
	slip := export[0]
	if _, err := xid.FromString(slip["uid"]); err != nil {
		t.Errorf("no valid uid found")
	}
	if slip["comment"] != "" {
		t.Errorf("Expected empty comment")
	}
	if slip["stop"] != "" {
		t.Errorf("expected empty stop time")
	}
	format := "20060102T150405Z"
	if slip["start"] != now.Format(format) {
		t.Errorf("Start not set correctly")
	}

}

func TestMain(m *testing.M) {
	rand.Seed(time.Now().UnixNano())
	os.Exit(m.Run())
}
