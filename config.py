import click
import os
from typing import Any

from dataclasses import dataclass, asdict
import json

APP_NAME = "wriker"
CONFIG_PATH = os.path.join(click.get_app_dir(APP_NAME), "config.json")


@dataclass(frozen=True)
class Config:
    token: str
    user: str

    @classmethod
    def from_file(cls, filename: str, need_user=True):
        with open(filename) as fh:
            data = json.load(fh)
        return Config(token=data["token"], user=data["user"])


def default_config():
    return Config.from_file(CONFIG_PATH)


def write(config: Config) -> Any:
    dirname = os.path.dirname(CONFIG_PATH)
    os.makedirs(dirname, exist_ok=True)
    with open(CONFIG_PATH, "w") as fh:
        json.dump(asdict(config), fh)
