import subprocess
import json

from api import Task


def start(task: Task):
    args = ["timew", "start", f"wrike-id:{task.id}", f"title:{task.title}"]
    subprocess.run(args)


def stop():
    args = ["timew", "stop"]
    subprocess.run(args)


def export():
    args = ["timew", "export", "today"]
    result = subprocess.run(args, capture_output=True)
    return json.loads(result.stdout.decode())
