import requests
from dataclasses import dataclass
import click
import pendulum

from config import Config

URL = "https://www.wrike.com/api/v4"


def get(endpoint, token):
    url = f"{URL}/{endpoint}"
    headers = {"Authorization": f"bearer {token}"}
    r = requests.get(url, headers=headers)
    assert r.status_code == 200
    return r.json()


def post(endpoint, token):
    url = f"{URL}/{endpoint}"
    headers = {"Authorization": f"bearer {token}"}
    r = requests.post(url, headers=headers)
    assert r.status_code == 200
    return r.json()


@dataclass(frozen=True)
class Task:
    id: str
    title: str
    link: str


@dataclass(frozen=True)
class API:
    token: str
    user: str

    @classmethod
    def from_config(cls, config: Config):
        return API(config.token, config.user)

    def tasks(self):
        endpoint = "tasks?subTasks=true&responsibles=['KUAIXTY3']"
        answer = get(endpoint, self.token)
        data = answer["data"]
        tasks = [Task(id=t["id"], title=t["title"], link=t["permalink"]) for t in data]
        return list(set(tasks))

    def create_timelog(self, taskid, duration):
        endpoint = f"tasks/{taskid}/timelogs"
        comment = "created by wriker"
        hours = duration / 3600
        trackedDate = pendulum.now().to_date_string()
        data = f"hours={hours}&trackedDate={trackedDate}&comment={comment}"
        answer = post(f"{endpoint}?{data}", token=self.token)
        return answer
