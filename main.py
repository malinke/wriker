#!/usr/bin/env python3
import click
from typing import Any
from dataclasses import dataclass
import pendulum

from api import API, get
import config
import timewarrior


@click.group()
def cli():
    pass


@dataclass(frozen=True)
class User:
    id: str
    name: str


@cli.command()
@click.option("--token", type=str)
def configure(token: str):
    if token is None:
        token = click.prompt("API token", type=str)
    # select user from list of known users
    answer = get("contacts", token)
    raw_users = answer["data"]
    users = [User(u["id"], f"{u['firstName']} {u['lastName']}") for u in raw_users]
    click.echo("\n".join(f"{i} : {u.name}" for i, u in enumerate(users)))
    account_index = click.prompt("enter account number", type=int)
    conf = config.Config(token, users[account_index].id)
    config.write(conf)


@cli.command()
def tasks():
    conf = config.default_config()
    api = API.from_config(conf)
    click.echo("\n".join([str(t) for t in api.tasks()]))


@cli.command()
def start():
    conf = config.default_config()
    api = API.from_config(conf)
    tasks = api.tasks()
    click.echo("\n".join(f"{i}:{t.title}" for i, t in enumerate(tasks)))
    task_index = click.prompt("task to start working on", type=int)
    task = tasks[task_index]
    timewarrior.start(task)


@cli.command()
def stop():
    timewarrior.stop()


@dataclass
class TimewarriorTask:
    wrike_id: str
    duration: int

    @classmethod
    def from_timewarrior(cls, data):
        start = pendulum.parse(data["start"])
        end = pendulum.parse(data["end"])
        wrike_id = [t for t in data["tags"] if "wrike-id" in t][0].split(":")[1]
        return TimewarriorTask(wrike_id, end.diff(start).in_seconds())


@cli.command()
def push():
    tw_tasks = [TimewarriorTask.from_timewarrior(t) for t in timewarrior.export()]

    click.echo(tw_tasks)
    conf = config.default_config()
    api = API.from_config(conf)
    for t in tw_tasks:
        api.create_timelog(t.wrike_id, t.duration)


if __name__ == "__main__":
    cli()
